<?php

include_once ("../MeetingValidation.php");

//use \Phppot\Member;
//require_once ("../class/Member.php");

$mv = new MeetingValidation();
//$member = new Member();



$mv->getUser();
$userdata = $mv->getUserBasics();
$userdata[] = basename($_SERVER['PHP_SELF'],'.php');
//$member->storeUser($userdata);




include("include/init.php");
//include_once("include/connex.php");

$lang = $_GET['lang'];
if(!isset($lang)) $lang = 'es';
/*

$q = "SELECT * FROM videos WHERE 1 ";
$q.= "AND privado = '1' ";

$videosList = $mysqli->query($q);
*/

/*
echo "<pre>";
var_dump($videosList);
echo "</pre>";
*/
/*
function mesEsp($mes) {
	$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	return $meses_ES[$mes-1];
  }
*/
?>
<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="./css/styles.css?v=2.0">


    <title>Congresos híbridos. Una nueva realidad.</title>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159234762-5"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-159234762-5');
	</script>

</head>

<body class="bg-home">

	<div class="container">
		<!--
		<div class="row header align-items-end">
			<div class="col-md-3">
            <img src="http://sepa2020.es/wp-content/themes/sepatv/images/logoHeader.svg" width="200"  alt="">
			</div>
			<div class="col-md-6">
				<img src="http://sepa2020.es/wp-content/themes/sepatv/images/logoHeader.svg" width="200"  alt="">
         </div>
			<div class="col-md-3">
				<img src="http://sepa2020.es/wp-content/themes/sepatv/images/logoHeader.svg" width="200"  alt="">
		 </div>
-->
		 <div class="row header pt-2 pt-md-4  align-items-end">
            <div class="col-6 col-md-2 text-left">
                <img src="http://sepa2020.es/wp-content/themes/sepatv/images/logoSepaTv.svg" alt="SepaTV" class="mt-3 mt-md-2" width="80">
            </div>

            <div class="col-md-8 text-center">
                <a class="logo" href="https://on-air.sepa2020.es/view/hall.php">
                    <img src="img/logoSepaOnAir.svg" alt="Sepa'20" width="300">
                </a>
            </div>
			<div class="col-md-2 text-right  p-4 p-md-0 d-none d-md-block pl-lg-4">
				<span class="es"><img src="img/logo20.svg" alt="Sepa'20" class=""></span>
            </div>


         <nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link btn btn-outline-primary" href="https://sepa.6connex.eu/event/sepaonair/login">Visita Expoperio</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link btn btn-outline-primary" href="https://sepa.6connex.eu/event/sepaonair/es-es#!/videoteca">Vídeos gratuitos Colaboradores</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link btn btn-outline-primary" href="#">Vídeos sesiones pago</a>
                </li>
              </ul>
              <span class="navbar-text">
				  <!--
               <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link btn btn-outline-secondary" href="#">ES <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link btn btn-outline-secondary" href="#">EN</a>
                  </li>
				</ul>
				-->
				<div class="col-12 text-right py-2">
					<div id="lang-selector" class="text-right">
						<a href="index.php?lang=es" class="btn btn-outline-dark <?php if($lang == 'es') echo 'active';?>" data-lang="es">ES</a>
						<a href="index.php?lang=en" class="btn btn-outline-dark <?php if($lang == 'en') echo 'active';?>" data-lang="en">EN</a>
					</div>
					<div class="cerrar-sesion text-right">
						<a href="/logout.php" class="btn Xbtn-dark"> <span aria-hidden="true">×</span> <span class="es">Cerrar sesión</span><span class="en d-none">Log out</span></a>
					</div>
				</div>
              </span>
            </div>
          </nav>

		</div>
	</div>




    <!-- Page Content -->
    <div id="page-content" class="container">


		<div class="row d-flex bg-hall">
			<?php
			while($row = mysqli_fetch_array($videosList)) {
			?>
			<div class="col-md-4 py-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title"> <?= strftime("%d",strtotime($row['fechaHora']))." ".mesEsp(date("n",strtotime($row['fechaHora'])));?></h5>
						<!--<p class="card-text"><strong>10.00-13.15</strong></p>-->
						<p class="card-text title-sesion"><strong><?= $row['titulo1'] ?></strong></p>
						<h4 class="subtitle"><?= $row['titulo2'] ?></h4>
						<p> </p>
						<p><br></p>
						<?php if($row['logo']){?>
							<img src="https://sepa2020.es/wp-content/themes/sepatv/images/logoOralB.svg" class="img-fluid">
							<p><br></p>
						<?php }?>
						<a href="<?php echo $row['urlEs']; ?>" class="btn btn-play btn-rounded-primary" target="_blank"><span>Reproducir</span>
							<span class="circle">
								<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-play-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path d="M11.596 8.697l-6.363 3.692c-.54.313-1.233-.066-1.233-.697V4.308c0-.63.692-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z"></path>
								</svg>
							</span>
						</a>
					</div>
				</div>
			</div>
			<?php
			}

			?>





		</div>



    </div><!-- //Page Content -->



    <div class="full-bg"></div>
    <!-- -->

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
		  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>


	<!-- ============= browser update ============ -->
	<!-- https://browser-update.org/customize.html -->
	<style>
		body .buorg{
			color: #000;
			font: 1.2rem Calibri, Helvetica, sans-serif;
			background-color: #fff430;
			box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
			border-bottom: 1px solid #fff430;
		}
		body .buorg-pad {
			padding: 30px 10px;
			line-height:1.4em;
		}
		body #buorgig,
		body #buorgul,
		body #buorgpermanent {
			color: #fff;
			box-shadow: none;
			padding: 0.2em 2em;
			border-radius: 0;
			font-weight: normal;
			background: #000000;
			margin: 0.4em;
		}
		body #buorgig {
			background-color: #000000;
		}
	</style>
	<script>
		var $buoop = {
			required:{e:-4,f:-3,o:-3,s:-1,c:-3},
			reminder:0,
			reminderClosed:1,
			insecure:true,
			api:2020.05,
			text: 'Su navegador, {brow_name}, requiere actualizarse para el correcto visionado.<br>Le recomendamos el uso de otros navegadores como Chrome o Firefox para un óptimo visionado de los eventos online.<br><a{up_but}>Actualizar</a>  <a{ignore_but}>Ignorar</a>'
			};
		function $buo_f(){
		 var e = document.createElement("script");
		 e.src = "//browser-update.org/update.min.js";
		 document.body.appendChild(e);
		};
		try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
		catch(e){window.attachEvent("onload", $buo_f)}
	</script>
	<!-- //=============== browser update ============= -->

</body>
</html>

