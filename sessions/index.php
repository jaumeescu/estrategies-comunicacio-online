<?php
/******************************************************************************
 * Muestra Videos desde la tabla "videos" con el campo privado <> 1
 * y con fecha igual o mayor que hoy
 *
 * 20/10/2020: V1.0
 *
 * Parámetos URL:
 * cat: Categoría (1=dentistes / 2=higienistes)
 * pat: Patrocinador
 *
 ********************************************************************************/

include("include/init.php");
include_once("include/connex.php");

$filtroCat = '0';
if(isset($_GET['cat'])) {
	$filtroCat 	= $_GET['cat'];	// Categoría (1=dentistes / 2=higienistes)
}
$filtroPat = '0';
if(isset($_GET['pat'])) {
	$filtroPat 	= $_GET['pat'];	// Patrocinador
}
$hoy = strftime("%Y-%m-%d 23:59:59");

$q = "SELECT * FROM videos WHERE 1 ";

if($filtroCat =='1') $q.="AND cat1 = '1' ";
if($filtroCat =='2') $q.="AND cat2 = '1' ";
//if($filtroPat) 		 $q.="AND patrocinador = '$filtroPat' ";
if($filtroPat) 		 $q.="AND  EXISTS (SELECT * FROM patrocinadores WHERE videos.id = patrocinadores.id_video AND patrocinadores.nombre ='$filtroPat' ) ";

$q.= "AND privado = '1' ";
$q.= "AND fechaHora <= '$hoy' ";

$q.= "ORDER BY fechaHora ";

$videosList = $mysqli -> query($q);
//echo "$q <br>";

function mesEsp ($mes) {
	$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	return $meses_ES[$mes-1];
}

?>

<!doctype html>
<html lang="es">
<head>
<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">


	<!-- Iconos Font Awesome  -->
	<script src="https://kit.fontawesome.com/626ece0c1b.js" crossorigin="anonymous"></script>
	<!-- /Iconos Font Awesome  -->

    <link rel="stylesheet" href="css/styles.css">


    <title>Sepa'20 · Videos</title>


</head>

<body class="bg-hall ">


	<div class="container-fluid">
		<div class="row header align-items-end">
			<img src="https://vod.on-air.sepa2020.es/img/infografias/header.png" class="img-top-right"  alt="">
			<div class="col-md-3">
				<div class="header-logo">
					<img src="http://sepa2020.es/wp-content/themes/sepatv/images/logoHeader.svg" width="200"  alt="">
					<p class="claim">Un Congreso para todos</p>
					<p><span class="dateLogo">11/SEP-28/NOV</span></p>
				</div>
			</div>
			<div class="col-md-9">
				<h1>Disfruta cuando y donde quieras<br/> de todos los vídeos exclusivos<br/>  incluidos en tu subscripción.</h1>
			</div>

			<div class="col-md-3"></div>
			<div class="col-md-9">
			<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link btn btn-outline-primary" href="https://sepa.6connex.eu/event/sepaonair/es-es#!/videoteca">Ver vídeos gratuitos sesiones colaborades</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link btn btn-outline-primary" href="https://sepa.6connex.eu/event/sepaonair/login">Expoperio, todas las novedades del sector</a>
                </li>
              </ul>
              <span class="navbar-text">
				  <!--
               <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link btn btn-outline-secondary" href="#">ES <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link btn btn-outline-secondary" href="#">EN</a>
                  </li>
				</ul>
				-->
				<!--
				<div class="col-12 text-right py-2">
					<div id="lang-selector" class="text-right">
						<a href="index.php?lang=es" class="btn btn-outline-dark <?php if($lang == 'es') echo 'active';?>" data-lang="es">ES</a>
						<a href="index.php?lang=en" class="btn btn-outline-dark <?php if($lang == 'en') echo 'active';?>" data-lang="en">EN</a>
					</div>
					<div class="cerrar-sesion text-right">
						<a href="/logout.php" class="btn Xbtn-dark"> <span aria-hidden="true">×</span> <span class="es">Cerrar sesión</span><span class="en d-none">Log out</span></a>
					</div>
				</div>
				-->
              </span>
            </div>
          </nav>
		  </div>

		</div>
	</div>

    <!-- Page Content -->
    <div id="page-content" class="container-fluid">


		<!-- SALAS -->


		<div class="row d-flex">

		<?php
		while($row = mysqli_fetch_array($videosList)) {
			$logo= isset($row['logo']) ? $row['logo'] : '';
		?>

			<div class="col-md-3 py-3">
				<div class="card">
					<!-- 16:9 aspect ratio
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="<?php echo $row['urlEs'] ?>"></iframe>
						</div>
					-->

					<div class='embed-container'>
						<iframe  class="video-responsive-item" src="<?php echo $row['urlEs'] ?>"  frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
					</div>
					<div class="card-body">


						<div>
							<span class="fecha">
								<?php

									echo strftime("%d",strtotime($row['fechaHora']))." ". mesEsp(date("n",strtotime($row['fechaHora'])));
								?>
							</span>
						</div>

						<p class="card-text titulo1"><strong><?php echo $row['titulo1'] ?></strong></p>

						<h4 class="subtitle"><?php echo $row['titulo2'] ?></h4>

						<p class="card-text titulo3"><strong><?php $row['titulo3'] ?></strong></p>

<!-- Canvis multiLogo 22/10/2020

						<p class="card-text "><strong><?php echo $row['id'] ?></strong></p>
						<?php $logo=($row['logo']);?>
						<img src="<?php echo $logo?>" class="logo img-fluid pull-left mr-2">
-->

						<?php  $q = "SELECT * FROM patrocinadores WHERE id_video = "; $q.=$row['id'];
							$patroList = $mysqli->query($q);
							if($patroList->num_rows){
								echo "<br>Colabora:<br>";
								while($rowP = mysqli_fetch_array($patroList)) {
									$logo=($rowP['logo']);?>
									<img src="<?php echo $logo?>" class="logo img-fluid pull-left mr-2">
								<?php }
							}?>
					</div>
				</div>
			</div>

		<?php
		}
		$videosList-> free_result();
		?>



		</div>


		<div class="row pt-5 pb-3">
			<div class="col-12 py-5">
			</div>
		</div>



	</div>
    <!-- -->


    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
		  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>






	<!-- =============== browser update ============= -->
	<style>
		body .buorg{
			color: #000;
			font: 1.2rem Calibri, Helvetica, sans-serif;
			background-color: #fff430;
			box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
			border-bottom: 1px solid #fff430;
		}
		body .buorg-pad {
			padding: 30px 10px;
			line-height:1.4em;
		}
		body #buorgig,
		body #buorgul,
		body #buorgpermanent {
			color: #fff;
			box-shadow: none;
			padding: 0.2em 2em;
			border-radius: 0;
			font-weight: normal;
			background: #000000;
			margin: 0.4em;
		}
		body #buorgig {
			background-color: #000000;
		}
    </style>


	<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
</script>




	<script>
		var $buoop = {
			required:{e:-4,f:-3,o:-3,s:-1,c:-3},
			reminder:0,
			reminderClosed:1,
			insecure:true,
			api:2020.05,
			text: 'Su navegador, {brow_name}, requiere actualizarse para el correcto visionado.<br>Le recomendamos el uso de otros navegadores como Chrome o Firefox para un óptimo visionado de los eventos online.<br><a{up_but}>Actualizar</a>  <a{ignore_but}>Ignorar</a>'
			};
		function $buo_f(){
		 var e = document.createElement("script");
		 e.src = "//browser-update.org/update.min.js";
		 document.body.appendChild(e);
		};
		try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
		catch(e){window.attachEvent("onload", $buo_f)}
	</script>
	<!-- //=============== browser update ============= -->


</body>

</html>

